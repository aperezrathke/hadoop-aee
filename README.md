# README #

This is a toy repository demonstrating Apache Hadoop 2.7.3 running multi-core (using YARN) on a single node (pseudo-distributed).

The [virtualbox](https://www.virtualbox.org/) machine can be downloaded from [here](https://www.dropbox.com/sh/kwlt4t3m5frkqg6/AABjxkE0MUyrpsVlXUvipahfa?dl=0)
(note: I have no idea how portable this machine is as it was **not** created using [vagrant](https://www.vagrantup.com/) or [docker](https://www.docker.com/))

In addition, this repository serves as a guide for installing and using [RHadoop](https://github.com/RevolutionAnalytics/RHadoop/wiki).

The RHadoop demo is a map-reduce implementation of the aggregated estimating equation (AEE) for logistic regression applied to the [airline](http://stat-computing.org/dataexpo/2009/) dataset.

For more information on AEE, please refer to:

Lin, Nan, and Ruibin Xi. "Aggregated estimating equation estimation." Statistics and Its Interface 4.1 (2011): 73-83.

### Files ###

* airline.trim.keyed.csv - The input dataset used by the demo R scripts. Only features first 1000 records of the airline dataset. Also, an ID column has been appended for mapping each record to its corresponding partition.
* append_row_id.sh - The bash script used for generating the keyed airline dataset.
* example_aee.local.R - R script for AEE logistic regression using RHadoop rmr2 local backend. This script allows breakpoints to be set in the map and reduce functions.
* example_aee.R - R script for AEE logistic regression using RHadoop rmr2 hadoop backend.
* Hadoop and R.pdf - A class presentation on Hadoop and R.
* Hadoop with R Guide.pdf - Detailed instructions for configuring Apache Hadoop 2.7.3 and RHadoop packages such as rhdfs and rmr2 on an Ubuntu virtualbox machine.
* Start-Hadoop-Fs.sh - Bash script for starting hdfs and YARN on a node with Hadoop installed according to the 'Hadoop with R Guide.pdf'
* Stop-Hadoop-Fs.sh - Bash script stopping hdfs and YARN on a node with Hadoop installed according to the 'Hadoop with R Guide.pdf'
* trim_data.sh - Bash script for keeping only the first set of rows from the full airline dataset (script used to generate airline.trim.keyed.csv)

### Usage ###

Follow the instructions in 'Hadoop with R Guide.pdf' to configure a sandbox environment (or obtain a pre-configured environment from [here](http://www.cloudera.com/), [here](http://hortonworks.com/), or [here](https://www.mapr.com/)).

The guide will also explain how to copy the dataset to hdfs and how to run the R scripts using RHadoop rmr2 package.