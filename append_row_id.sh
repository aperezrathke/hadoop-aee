awk -F',' -v OFS=',' '
  NR == 1 {print "ID", $0; next}
  {print (NR-1), $0}
' airline.csv > airline.keyed.csv

