#!/usr/bin/env bash

echo "Starting Hadoop..."

# Start Hadoop distributed file system
start-dfs.sh

##############################
# Uncomment if using YARN

# Start YARN resource manager
start-yarn.sh

##############################
# Uncomment if using hbase

# Start Hadoop database
#start-hbase.sh

# Start HBase Thrift server
#nohup hbase thrift start &> ~/thrift.logs.txt &

echo "Hadoop started."

