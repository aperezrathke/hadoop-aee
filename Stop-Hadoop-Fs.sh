#!/usr/bin/env bash

echo "Stopping Hadoop..."

##############################
# Uncomment if using hbase

# Stop Hadoop database
#stop-hbase.sh

##############################
# Uncomment if using yarn

# Stop YARN resource manager
stop-yarn.sh

# Stop Hadoop distributed file system
stop-dfs.sh

echo "Hadoop stopped."

